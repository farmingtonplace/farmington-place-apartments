Farmington Place Apartments is the perfect place to call home in Wichita. These newly renovated apartments are available in studio, 1 and 2 bedroom floor plans. This apartment community is in lush green lawns with a sparkling swimming pool, a children's playground, a tennis court and clubhouse.

Address: 6801 W Par Lane 900, Wichita, KS 67212, USA

Phone: 316-942-8195
